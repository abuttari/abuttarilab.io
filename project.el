(add-to-list 'load-path "~/.emacs.d/elpa/htmlize-20180923.1829")
(require 'org)
(require 'htmlize)
(require 'font-lock)
(require 'ox-html)

(setq org-html-htmlize-output-type 'css)

(require 'ox-latex)
(setq org-latex-listings 'listings) 


;; Fontify the source code (require 'htmlize)
(setq org-src-fontify-natively t)

(setq org-confirm-babel-evaluate nil)

(org-babel-do-load-languages
 'org-babel-load-languages
 '((C . t)
   (emacs-lisp . t)
   (R . t)
   (shell . t)
   (latex . t)
   (gnuplot . t)
   (fortran . t)))

(require 'ox-publish)
(setq org-publish-project-alist
      '(("mysite"
         :base-directory "."
         :base-extension "org\\|html"
         :recursive t
         :section-numbers nil
         :table-of-contents nil
         :publishing-directory "./public"
         :publishing-function org-html-publish-to-html
         :auto-postamble nil
         :html-postamble " "
         :style "")
        ("imgs"
         :base-directory "./imgs"
         :base-extension "jpg\\|gif\\|png"
         :publishing-directory "./public/imgs"
         :publishing-function org-publish-attachment
         :recursive t)
        ("etc"
         :base-directory "./aux"
         :base-extension "css\\|bib\\|el"
         :publishing-directory "./public/aux"
         :publishing-function org-publish-attachment)
        ("docs"
         :base-directory "./docs"
         :base-extension "html\\|tex\\|bib\\|pdf\\|c\\|F90\\|ics\\|tgz"
         :publishing-directory "./public/docs"
         :publishing-function org-publish-attachment)
        ("mywebsite" :components ("mysite" "imgs" "etc" "docs"))))
