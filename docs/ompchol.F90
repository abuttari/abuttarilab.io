! ompchol is meant to show different ways of multithreading a simple
! operation such as the Cholesky factorization. The LAPACK and BLAS
! libraries are needed to compile and run this code.
!
!
! This code is incomplete and subotpimal on purpose to incourage
! students to play with it, optimize, fill-up the missing
! parts/features.


program ompchol
  implicit none

  integer, parameter :: chol_busy_ =-huge(0)
  integer, parameter :: chol_pnl_  = 1
  integer, parameter :: chol_trs_  = 2
  integer, parameter :: chol_upd_  = 3
  integer, parameter :: chol_none_ = 0
  integer, parameter :: chol_done_ = -1

  type task
     ! the task data type is meant to define computational tasks of a
     ! 2D blocked Cholesky factorization
     integer :: id    ! the type of task to be performed. 
     integer :: row   ! the row-block index of the block to be treated
     integer :: col   ! the col-block index of the block to be treated
     integer :: step  ! the block-step index of the factorization
     integer, pointer :: tab(:,:) ! the progress table that keep track
                                  ! of the tasks perfomed during the
                                  ! facto
  end type task


  real(kind(1.d0)), allocatable :: a1(:,:), a2(:,:), a3(:,:), a4(:,:)
  integer                       :: n, nb, len, info, i, nargs
  character(LEN=20)             :: str
  integer                       :: t_start, t_end, t_rate
  real(kind(1.d0))              :: flops

  nargs = command_argument_count()
  if(nargs .lt. 2) then
     write(*,'("Usage:")')
     write(*,'("./ompchol n nb")')
     write(*,'("where n is the size of the matrix ")')
     write(*,'("and nb is the block size. ")')
     stop
  end if

  call get_command_argument(1,value=str,length=len)
  read(str(1:len),*)n

  call get_command_argument(2,value=str,length=len)
  read(str(1:len),*)nb

  allocate(a1(n,n), a2(n,n), a3(n,n), a4(n,n))

  ! fill up A with random numbers
  call random_number(a1)
  ! make it positive definite
  do i=1, n
     a1(i,i)=n
  end do

  ! The Cholesky factorization costs (n^3)/3
  flops = real(n)*real(n)*real(n)/3.d0

  a2 = a1
  a3 = a1
  a4 = a1

  ! call to lapack Cholesky fatorization
  call system_clock(t_start)
  call dpotrf('l', n, a1, n, info)
  call system_clock(t_end, t_rate)
  write(*,'("LAPACK   -- GFlops:",f7.2)')flops/(real(t_end-t_start)/real(t_rate))/1e9

  ! call fo fork-join parallel Cholesky
  call system_clock(t_start)
  call fj_chol_poor('l', n, nb, a4, info)
  call system_clock(t_end, t_rate)
  write(*,'("FJ-CHOLP -- GFlops:",f7.2,"   Error:",es10.3)')&
       & flops/(real(t_end-t_start)/real(t_rate))/1e9,maxval(abs(a1-a4))/maxval(abs(a1))

  ! call fo fork-join parallel Cholesky
  call system_clock(t_start)
  call fj_chol_good('l', n, nb, a2, info)
  call system_clock(t_end, t_rate)
  write(*,'("FJ-CHOLG -- GFlops:",f7.2,"   Error:",es10.3)')&
       & flops/(real(t_end-t_start)/real(t_rate))/1e9,maxval(abs(a1-a2))/maxval(abs(a1))

  ! call fo fork-join parallel Cholesky
  call system_clock(t_start)
  call dyn_chol('l', n, nb, a3, info)
  call system_clock(t_end, t_rate)
  write(*,'("DYN-CHOL -- GFlops:",f7.2,"   Error:",es10.3)')&
       & flops/(real(t_end-t_start)/real(t_rate))/1e9,maxval(abs(a1-a3))/maxval(abs(a1))

stop

contains


  subroutine fj_chol_poor(uplo, n, nb, a, info)
    use omp_lib
    implicit none
    real(kind(1.d0)) :: a(:,:)
    integer          :: n, info, nb
    character        :: uplo

    integer          :: i, k, j, s

    if(uplo .eq. 'l') then
       do k=1, n, nb
          s = min(nb, n-k+1)
          ! factoriza a diagonal block
          call dpotrf(uplo, s, a(k,k), n, info)
          
          do i=k+nb, n, nb
             s = min(nb, n-i+1)
             ! update a block along the same column as the last diagonal facto 
             call dtrsm('r', 'l', 't', 'n', s, nb, 1.d0, a(k,k), n, a(i,k), n)
             !$omp parallel do private(j)
             do j=k+nb, i, nb
                ! update the trailing submatrix
                if (j .eq. i) then
                   call dsyrk('l', 'n', s, nb, -1.d0, a(i,k), n, 1.d0, a(i,i), n)
                else
                   call dgemm('n', 't', s, nb, nb, -1.d0, a(i,k), n, a(j,k), n, 1.d0, a(i,j), n)
                end if
             end do
             !$omp end parallel do
          end do
          
       end do
    else if (uplo .eq. 'u') then
       write(*,'("Not implemented. Excercise for the intelligent student ;-)")')
    end if
    
    return
  end subroutine fj_chol_poor


  subroutine fj_chol_good(uplo, n, nb, a, info)
    use omp_lib
    implicit none
    real(kind(1.d0)) :: a(:,:)
    integer          :: n, info, nb
    character        :: uplo

    integer          :: i, k, j, s

    if(uplo .eq. 'l') then
       do k=1, n, nb
          s = min(nb, n-k+1)
          ! factoriza a diagonal block
          call dpotrf(uplo, s, a(k,k), n, info)

          !$omp parallel private(i, s, j)
          !$omp do
          do i=k+nb, n, nb
             s = min(nb, n-i+1)
             ! update a block along the same column as the last diagonal facto 
             call dtrsm('r', 'l', 't', 'n', s, nb, 1.d0, a(k,k), n, a(i,k), n)
          end do
          !$omp end do
          
          !$omp do
          do i=k+nb, n, nb
             s = min(nb, n-i+1)
             do j=k+nb, i, nb
                ! update the trailing submatrix
                if (j .eq. i) then
                   call dsyrk('l', 'n', s, nb, -1.d0, a(i,k), n, 1.d0, a(i,i), n)
                else
                   call dgemm('n', 't', s, nb, nb, -1.d0, a(i,k), n, a(j,k), n, 1.d0, a(i,j), n)
                end if
             end do
          end do
          !$omp end do
          !$omp end parallel 
       end do
    else if (uplo .eq. 'u') then
       write(*,'("Not implemented. Excercise for the intelligent student ;-)")')
    end if
    
    return
  end subroutine fj_chol_good


  
  subroutine dyn_chol(uplo, n, nb, a, info)
    use omp_lib
    implicit none
    real(kind(1.d0)) :: a(:,:)
    integer          :: n, info, nb
    character        :: uplo

    type(task) :: tsk
    integer, allocatable, target    :: ptab(:,:)
    integer :: s

    s = (n-1)/nb+1
    allocate(ptab(s,s))
    ptab = 0
    
    !$omp parallel private(tsk)
    tsk%tab => ptab
    ! threads keep spinning in this loop, fetching tasks and executing
    ! the corresponding action, until the chol_done_ task is
    ! return. At this time all the tasks leave the loop and join at
    ! the end of the paralle region
    do
       ! check fo a new task to perform
       call get_task(tsk, n, nb, uplo)
       select case(tsk%id)
          case (chol_none_)  ! task not found
             cycle
          case (chol_done_)  ! facto completed, exit
             exit
          case (chol_pnl_)   ! panel
             call pnl(tsk, a, n, nb, uplo, info)
          case (chol_trs_)   ! trsm
             call trs(tsk, a, n, nb, uplo, info)
          case (chol_upd_)   ! upd
             call upd(tsk, a, n, nb, uplo, info)
       end select
    end do
    !$omp end parallel

    return
  end subroutine dyn_chol

  
  subroutine get_task(tsk, n, nb, uplo)
    ! This subroutine is the heart of the dynamic cholesky
    ! factorization. It checks whether there is a task to perform by
    ! looking at the progress table which keeps track of the state of
    ! the factorization. The progress table is initialized to 0 and
    ! then each value ptab(i,j) is set to the value k as soon as the
    ! (i,j) block is up to date with respect to the factorization step
    ! k.
    !
    ! Whenever a task on a block (i,j) is found, the corresponfing
    ! entry in the progress table is set to chol_busy_ so that no
    ! other thread can pick the same task.
    !
    ! Accesses to the progress table have to be protected with a
    ! critical region to prevent multiple threads to look for a task
    ! at the same time or to write on the progress table when a task
    ! is completed; this is obviously done to avoid conflicts.
    use omp_lib
    implicit none
    type(task), intent(out) :: tsk
    integer    :: n, nb
    character  :: uplo
    integer :: i, j, k, lastp

    if(uplo .eq. 'u') then
       write(*,'("Not implemented. Excercise for the intelligent student ;-)")')
       tsk%id=-1
       return
    end if

    ! set the task id to "not found" by default. It will be
    ! overwritten below if there is a task available.
    tsk%id = 0

    ! lastp is the size in blocks of the matrix
    lastp = (n-1)/nb+1
    
    ! shortcuts

    ! check is the facto has just started; in this case the entry
    !(0,0) of the progress table is still set to 0
    !$omp critical (tab)
    if(tsk%tab(1,1) .eq. 0) then
       tsk%tab(1,1) = chol_busy_
       tsk = task(id=chol_pnl_, row=0, col=0, step=1, tab=tsk%tab)
    end if
    !$omp end critical (tab)
    if(tsk%id .gt. 0) return ! if a task was found, return; otherwise keep going

    ! if the following condition is true, then there is only one task
    ! left to do: the factorization of the last diagonal block. The
    ! condition checks whether the last diagonal block is up-to-date
    ! with the respect to the previous one, i.e. it is ready for being
    ! factorized
    !$omp critical (tab)
    if(tsk%tab(lastp,lastp) .eq. lastp-1) then
       tsk%tab(lastp,lastp) = chol_busy_
       tsk = task(id=chol_pnl_, row=0, col=0, step=lastp, tab=tsk%tab)
    end if
    !$omp end critical (tab)
    if(tsk%id .gt. 0) return ! if a task was found, return; otherwise keep going

    ! if the last diagonal block is up-to-date with respect to itself,
    ! then it means that there is nothing left to do. The task
    ! returned is chol_done_
    !$omp critical (tab)
    if(tsk%tab(lastp,lastp) .eq. lastp) then
       tsk = task(id=chol_done_, row=0, col=0, step=0, tab=tsk%tab)
    end if
    !$omp end critical (tab)
    if(tsk%id .gt. 0) return ! if a task was found, return; otherwise keep going

    ! now we look for any other "non special" task

    ! first check whether it is possible to make a panel facto
    !$omp critical (tab)
    do i=1, lastp
       ! for each diagonal block, we check whether it is up-to-date
       ! wrt the previous step. In this case it can be factorized.
       if(tsk%tab(i,i) .eq. i-1) then
          tsk%tab(i,i) = chol_busy_
          tsk = task(id=chol_pnl_, row=0, col=0, step=i, tab=tsk%tab)
          goto 10
       end if
    end do
10  continue
    !$omp end critical (tab)
    if(tsk%id .gt. 0) return ! if a task was found, return; otherwise keep going

    ! second check for a trsm
    !$omp critical (tab)
    do k=1, lastp-1
       ! loop over all the steps k.  
       ! If the (k,k) block has not been factorized, then it is not
       ! worth to look for a trsm along the corresponding column.
       if(tsk%tab(k,k) .ne. k) cycle
       do i=k+1, lastp
          ! now, if the (k,k) diagonal block has been factorized, we
          ! may look for a block along the k-th block-column on which
          ! to make a trsm. The trsm can be done on the block (i,k)
          ! only in the case where the block is up-to-date with
          ! respect to the step k-1 
          if(tsk%tab(i,k) .eq. k-1) then
             tsk%tab(i,k) = chol_busy_
             tsk = task(id=chol_trs_, row=i, col=0, step=k, tab=tsk%tab)
             goto 20
          end if
       end do
    end do
20  continue
    !$omp end critical (tab)
    if(tsk%id .gt. 0) return ! if a task was found, return; otherwise keep going

    ! third check for an upd
    !$omp critical (tab)
    do k=1, lastp-1
       ! here we check whether we can do the update of a block (i,j)
       ! with respect to step k.

       ! if the k-th diagonal block has not been factorized, then it
       ! is not worth checking for the update; cycle.
       if(tsk%tab(k,k) .ne. k) cycle 
       do i=k+1, lastp
          ! if the trsm of the (i,k) block has not been done, we
          ! won't be able to do the update; cycle.
          if((tsk%tab(i,k) .ne. k)) cycle
          do j=k+1, i
             ! if the trsm of the (j,k) block has not been done, we
             ! won't be able to do the update; cycle.
             if((tsk%tab(j,k) .ne. k)) cycle
             ! finally, we can do the k-th update of the block (i,j)
             ! only in the case where the block is up-to-date with
             ! respect to step k-1.
             if(tsk%tab(i,j) .eq. k-1) then
                tsk%tab(i,j) = chol_busy_
                tsk = task(id=chol_upd_, row=i, col=j, step=k, tab=tsk%tab)
                goto 30
             end if
          end do
       end do
    end do
30  continue
    !$omp end critical (tab)

    
    return
  end subroutine get_task




  subroutine pnl(tsk, a, n, nb, uplo, info)
    ! this subroutine does the Cholesky factorization of the
    ! (tsk%step,tsk%step) diagonal block of the matrix a.
    use omp_lib
    implicit none
    type(task) :: tsk
    real(kind(1.d0)) :: a(:,:)
    integer          :: n, info, nb
    character        :: uplo
    integer          :: id
    integer          :: i, j, k, s

    ! k is the index of the first row and first column of the
    ! (tsk%step,tsk%step) diagonal block. 
    k = 1+nb*(tsk%step-1)

    ! s is the size of the diagonal block. It may not be equal to nb
    ! in the case where the size of the matrix a is not a multiple of
    ! nb.
    s = min(nb, n-k+1)

    id=0
    !$ id = omp_get_thread_num()
#if defined (dbg)
    write(*,'(i2," -- PNL    k:",i4)')id, k
#endif
    ! call the LAPACK Cholesky factorization on the diagonal block
    call dpotrf(uplo, s, a(k,k), n, info)

    ! set the diagonal block up-to-date with respect to itself
    !$omp critical (tab)
    tsk%tab(tsk%step,tsk%step) = tsk%step
    !$omp end critical (tab)
    
    return
  end subroutine pnl

  subroutine trs(tsk, a, n, nb, uplo, info)
    ! this subroutine does the trsm operation on the block
    ! (tsk%row,tsk%step) using the factor resulting from the Cholesky
    ! factorization of the (tsk%step,tsk%step) diagonal block
    use omp_lib
    implicit none
    type(task) :: tsk
    real(kind(1.d0)) :: a(:,:)
    integer          :: n, info, nb
    character        :: uplo
    integer          :: id
    integer          :: i, j, k, s

    ! k is the index of the first row and first column in the
    ! (tsk%step,tsk%step) diagonal block and the first column of the
    ! (tsk%row,tsk%step) block
    k = 1+nb*(tsk%step-1)

    ! i is the index of the first row of the (tsk%row,tsk%step) block.
    i = 1+nb*(tsk%row-1)

    ! s is the number of rows in the (tsk%row,tsk%step) block. It may
    ! not be equal to nb in the case where the size of the matrix a is
    ! not a multiple of nb.
    s = min(nb, n-i+1)
    id=0
    !$ id = omp_get_thread_num()
#if defined (dbg)
    write(*,'(i2," -- RSM    k:",i4,"  i:",i4)')id, k, i
#endif
    ! do the trsm
    call dtrsm('r', 'l', 't', 'n', s, nb, 1.d0, a(k,k), n, a(i,k), n)

    ! set the (tsk%row,tsk%step) block up-to-date with respect to step
    ! tsk%step 
    !$omp critical (tab)
    tsk%tab(tsk%row,tsk%step) = tsk%step
    !$omp end critical (tab)
    
    return
  end subroutine trs

  subroutine upd(tsk, a, n, nb, uplo, info)
    ! this subroutine does the update of a (tsk%row,tsk%col) block
    ! with respect to step tsk%step using blocks (tsk%row,tsk%step)
    ! and (tsk%col,tsk%step).
    use omp_lib
    implicit none
    type(task) :: tsk
    real(kind(1.d0)) :: a(:,:)
    integer          :: n, info, nb
    character        :: uplo
    integer          :: id
    integer          :: i, j, k, s

    ! k is the index of the first column of the
    ! (tsk%row,tsk%step) and (tsk%col,tsk%step) blocks.
    k  = 1+nb*(tsk%step-1)

    ! i is the index of the first row of the
    ! (tsk%row,tsk%step) and (tsk%row,tsk%col) blocks. 
    i  = 1+nb*(tsk%row-1)

    ! j is the index of the first row in the (tsk%col,tsk%step) block
    ! and of the first column in the (tsk%row,tsk%col) block
    j  = 1+nb*(tsk%col-1)

    ! s is the number of rows in the (tsk%row,tsk%step) block. It may
    ! not be equal to nb in the case where the size of the matrix a is
    ! not a multiple of nb.
    s  = min(nb, n-i+1)
    id   = 0
    !$ id = omp_get_thread_num()
#if defined (dbg)
    write(*,'(i2," -- UPD    k:",i4,"  i:",i4,"  j:",i4)')id, k, i, j
#endif
    if (i .eq. j) then
       ! if the block to be updated is a diagonal block the use a
       ! symmetric rank-k update.
       call dsyrk('l', 'n', s, nb, -1.d0, a(i,k), n, 1.d0, a(i,i), n)
    else
       ! otherwise just a matrix-matrix product
       call dgemm('n', 't', s, nb, nb, -1.d0, a(i,k), n, a(j,k), n, 1.d0, a(i,j), n)
    end if
    ! set the (tsk%row,tsk%step) block up-to-date with respect to step
    ! tsk%step 
    !$omp critical (tab)
    tsk%tab(tsk%row,tsk%col) = tsk%step
    !$omp end critical (tab)
    
    return
  end subroutine upd

end program ompchol



















